package com.example.mapwithmarker;

import java.io.Serializable;

import androidx.annotation.NonNull;

public class Meliponario implements Serializable {
    int id;
    double lat;
    double lon;
    String beeSpecies;
    String products;

    public Meliponario(int id, double lat, double lon, String beeSpecies, String products) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.beeSpecies = beeSpecies;
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getBeeSpecies() {
        return beeSpecies;
    }

    public void setBeeSpecies(String beeSpecies) {
        this.beeSpecies = beeSpecies;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString();
    }
}
