package com.example.mapwithmarker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MarkActivity extends AppCompatActivity  implements View.OnClickListener {
    private View goToMapView;
    private Meliponario mMeliponario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark);

        mMeliponario = (Meliponario) getIntent().getSerializableExtra("meliponario");

        TextView coordinates = findViewById(R.id.coordinates);
        TextView beeSpecies = findViewById(R.id.bee_species);
        TextView products = findViewById(R.id.marker_products);

        if (mMeliponario != null) {
            coordinates.setText(mMeliponario.getLat() + "\n" + mMeliponario.getLon());
            beeSpecies.setText(mMeliponario.getBeeSpecies());
            products.setText(mMeliponario.getProducts());
        }

        goToMapView = findViewById(R.id.link_map_id);
        goToMapView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.link_map_id:
                if (mMeliponario != null) {
                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + mMeliponario.getLat() + "," + mMeliponario.getLon());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    if (mapIntent.resolveActivity(getPackageManager()) != null) {
                        startActivity(mapIntent);
                    }
                }
                break;
        }
    }
}