package com.example.mapwithmarker;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.webkit.WebView;

public class ProductionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production);

        WebView webViewOne = findViewById(R.id.production_web_1);
        WebView webViewTwo = findViewById(R.id.production_web_2);
        WebView webViewThr = findViewById(R.id.production_web_3);
        WebView webViewFou = findViewById(R.id.production_web_4);
        WebView webViewFiv = findViewById(R.id.production_web_5);

        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: 'MyFont';src: url(\"file:///android_asset/fonts/Cabin-Medium.ttf\")}body {font-family: 'MyFont';font-size: medium;text-align: justify;}</style></head><body><p align=\"justify\">";
        String pas = "</p></body></html>";

        String text1 = getString(R.string.production_text_1);
        String myHtmlString1 = pish + text1 + pas;
        webViewOne.loadDataWithBaseURL(null,myHtmlString1, "text/html", "UTF-8", null);
        webViewOne.setBackgroundColor(Color.TRANSPARENT);
        webViewOne.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        String text2 = getString(R.string.production_text_2);
        String myHtmlString2 = pish + text2 + pas;
        webViewTwo.loadDataWithBaseURL(null,myHtmlString2, "text/html", "UTF-8", null);
        webViewTwo.setBackgroundColor(Color.TRANSPARENT);
        webViewTwo.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        String text3 = getString(R.string.production_text_3);
        String myHtmlString3 = pish + text3 + pas;
        webViewThr.loadDataWithBaseURL(null,myHtmlString3, "text/html", "UTF-8", null);
        webViewThr.setBackgroundColor(Color.TRANSPARENT);
        webViewThr.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        String text4 = getString(R.string.production_text_4);
        String myHtmlString4 = pish + text4 + pas;
        webViewFou.loadDataWithBaseURL(null,myHtmlString4, "text/html", "UTF-8", null);
        webViewFou.setBackgroundColor(Color.TRANSPARENT);
        webViewFou.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        String text5 = getString(R.string.production_text_5);
        String myHtmlString5 = pish + text5 + pas;
        webViewFiv.loadDataWithBaseURL(null,myHtmlString5, "text/html", "UTF-8", null);
        webViewFiv.setBackgroundColor(Color.TRANSPARENT);
        webViewFiv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }
}