package com.example.mapwithmarker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        CardView cardMap = findViewById(R.id.card_map);
        CardView cardAbout = findViewById(R.id.card_about);
        CardView cardProduction = findViewById(R.id.card_production);
        CardView cardMoreInfo = findViewById(R.id.card_more_info);
        CardView cardGroup = findViewById(R.id.card_group);
        CardView cardReferences = findViewById(R.id.card_references);
        cardMap.setOnClickListener(this);
        cardAbout.setOnClickListener(this);
        cardMoreInfo.setOnClickListener(this);
        cardProduction.setOnClickListener(this);
        cardGroup.setOnClickListener(this);
        cardReferences.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.card_map:
                intent = new Intent(this, MapsMarkerActivity.class);
                break;
            case R.id.card_about:
                intent = new Intent(this, AboutActivity.class);
                break;
            case R.id.card_production:
                intent = new Intent(this, ProductionActivity.class);
                break;
            case R.id.card_more_info:
                intent = new Intent(this, MoreInfoActivity.class);
                break;
            case R.id.card_group:
                intent = new Intent(this, GroupActivity.class);
                break;
            case R.id.card_references:
                intent = new Intent(this, ReferencesActivity.class);
                break;
            default:
                intent = new Intent(this, MapsMarkerActivity.class);
        }
        startActivity(intent);
    }
}