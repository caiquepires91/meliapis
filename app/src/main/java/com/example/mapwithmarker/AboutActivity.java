package com.example.mapwithmarker;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.webkit.WebView;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        WebView webViewOne = findViewById(R.id.about_web_1);
        WebView webViewTwo = findViewById(R.id.about_web_2);

        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: 'MyFont';src: url(\"file:///android_asset/fonts/Cabin-Medium.ttf\")}body {font-family: 'MyFont';font-size: medium;text-align: justify;}</style></head><body><p align=\"justify\">";
        String pas = "</p></body></html>";

        String text1 = getString(R.string.about_text_1);
        String myHtmlString1 = pish + text1 + pas;
        webViewOne.loadDataWithBaseURL(null,myHtmlString1, "text/html", "UTF-8", null);
        webViewOne.setBackgroundColor(Color.TRANSPARENT);
        webViewOne.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        String text2 = getString(R.string.about_text_2);
        String myHtmlString2 = pish + text2 + pas;
        webViewTwo.loadDataWithBaseURL(null,myHtmlString2, "text/html", "UTF-8", null);
        webViewTwo.setBackgroundColor(Color.TRANSPARENT);
        webViewTwo.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }
}