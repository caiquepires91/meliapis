package com.example.mapwithmarker;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.webkit.WebView;
import android.widget.TextView;

public class GroupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        TextView email = findViewById(R.id.email);
        email.setText(Html.fromHtml("<a href=\"mailto:caiquepires91@gmail.com\">Clique aqui para entrar em contato por e-mail.</a>"));
        email.setMovementMethod(LinkMovementMethod.getInstance());

        TextView phone = findViewById(R.id.phone);
        phone.setText(Html.fromHtml("<a href=\"tel:74991104890\">(74) 99111-04890</a>"));
        phone.setMovementMethod(LinkMovementMethod.getInstance());

        TextView wpp = findViewById(R.id.wpp);
        wpp.setText(Html.fromHtml("<a href=\"https://wa.me/5574991104890?text=Te%20encontrei%20no%20MeliApis\">Clique aqui para mandar um Whatsapp</a>"));
        wpp.setMovementMethod(LinkMovementMethod.getInstance());

        /*https://wa.me/15551234567?text=I'm%20interested%20in%20your%20car%20for%20sale*/

        WebView webViewOne = findViewById(R.id.group_web_1);

        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: 'MyFont';src: url(\"file:///android_asset/fonts/Cabin-Medium.ttf\")}body {font-family: 'MyFont';font-size: medium;text-align: justify;}</style></head><body><p align=\"justify\">";
        String pas = "</p></body></html>";

        String text1 = getString(R.string.group_text_1);
        String myHtmlString1 = pish + text1 + pas;
        webViewOne.loadDataWithBaseURL(null,myHtmlString1, "text/html", "UTF-8", null);
        webViewOne.setBackgroundColor(Color.TRANSPARENT);
        webViewOne.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }
}