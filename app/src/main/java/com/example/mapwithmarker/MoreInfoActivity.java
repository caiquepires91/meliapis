package com.example.mapwithmarker;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

public class MoreInfoActivity extends AppCompatActivity implements View.OnClickListener {
    private AlertDialog.Builder mBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_info);

        mBuilder = new AlertDialog.Builder(this);
        TextView more_info_1 = findViewById(R.id.more_info_text_1);
        TextView more_info_2 = findViewById(R.id.more_info_text_2);
        TextView more_info_3 = findViewById(R.id.more_info_text_3);
        TextView more_info_4 = findViewById(R.id.more_info_text_4);
        TextView more_info_5 = findViewById(R.id.more_info_text_5);
        more_info_1.setOnClickListener(this);
        more_info_2.setOnClickListener(this);
        more_info_3.setOnClickListener(this);
        more_info_4.setOnClickListener(this);
        more_info_5.setOnClickListener(this);

        /*WebView webViewOne = findViewById(R.id.more_info_web_1);
        WebView webViewTwo = findViewById(R.id.more_info_web_2);
        WebView webViewThr = findViewById(R.id.more_info_web_3);
        WebView webViewFou = findViewById(R.id.more_info_web_4);
        WebView webViewFiv = findViewById(R.id.more_info_web_5);

        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: 'MyFont';src: url(\"file:///android_asset/fonts/Cabin-Medium.ttf\")}body {font-family: 'MyFont';font-size: medium;text-align: justify;}</style></head><body><p align=\"justify\">";
        String pas = "</p></body></html>";

        /*
        String text1 = getString(R.string.more_info_text_1);
        String myHtmlString1 = pish + text1 + pas;
        webViewOne.loadDataWithBaseURL(null,myHtmlString1, "text/html", "UTF-8", null);
        webViewOne.setBackgroundColor(Color.TRANSPARENT);
        webViewOne.setLayerType(WebView.LAYER_TYPE_NONE, null); */

        /*

        String text2 = getString(R.string.more_info_text_2);
        String myHtmlString2 = pish + text2 + pas;
        webViewTwo.loadDataWithBaseURL(null,myHtmlString2, "text/html", "UTF-8", null);
        webViewTwo.setBackgroundColor(Color.TRANSPARENT);
        webViewTwo.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        String text3 = getString(R.string.more_info_text_3);
        String myHtmlString3 = pish + text3 + pas;
        webViewThr.loadDataWithBaseURL(null,myHtmlString3, "text/html", "UTF-8", null);
        webViewThr.setBackgroundColor(Color.TRANSPARENT);
        webViewThr.setLayerType(WebView.LAYER_TYPE_NONE, null);

        String text4 = getString(R.string.more_info_text_4);
        String myHtmlString4 = pish + text4 + pas;
        webViewFou.loadDataWithBaseURL(null,myHtmlString4, "text/html", "UTF-8", null);
        webViewFou.setBackgroundColor(Color.TRANSPARENT);
        webViewFou.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        String text5 = getString(R.string.more_info_text_5);
        String myHtmlString5 = pish + text5 + pas;
        webViewFiv.loadDataWithBaseURL(null,myHtmlString5, "text/html", "UTF-8", null);
        webViewFiv.setBackgroundColor(Color.TRANSPARENT);
        webViewFiv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        */
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.more_info_text_1:
                mBuilder.setTitle("Propriedades do Mel");
                mBuilder.setMessage(R.string.more_info_text_1);
            break;
            case R.id.more_info_text_2:
                mBuilder.setTitle("Benefícios do Pólen Apícola");
                mBuilder.setMessage(R.string.more_info_text_2);
                break;
            case R.id.more_info_text_3:
                mBuilder.setTitle("Propriedades da Própolis");
                mBuilder.setMessage(R.string.more_info_text_3);
                break;
            case R.id.more_info_text_4:
                mBuilder.setTitle("Propriedades da Geleia Real");
                mBuilder.setMessage(R.string.more_info_text_4);
                break;
            case R.id.more_info_text_5:
                mBuilder.setTitle("Propriedades da Apitoxina");
                mBuilder.setMessage(R.string.more_info_text_5);
                break;
        }
        AlertDialog dialog = mBuilder.create();
        dialog.show();
    }
}