// Copyright 2020 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.example.mapwithmarker;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * An activity that displays a Google map with a marker (pin) to indicate a particular location.
 */
// [START maps_marker_on_map_ready]
public class MapsMarkerActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    // [START_EXCLUDE]
    // [START maps_marker_get_map_async]
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_maps);

        // Get the SupportMapFragment and request notification when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }
    // [END maps_marker_get_map_async]
    // [END_EXCLUDE]

    // [START_EXCLUDE silent]
    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    // [END_EXCLUDE]
    // [START maps_marker_on_map_ready_add_marker]
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // [START_EXCLUDE silent]
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        // [END_EXCLUDE]
        googleMap.setOnMarkerClickListener(this);

        LatLng location_1 = new LatLng(-9.5048055555556,-40.777694444444);
        googleMap.addMarker(new MarkerOptions()
            .position(location_1)
            .title("MeliApis")
        ).setTag(0);
        // [START_EXCLUDE silent]

        // Location 2
        LatLng location_2 = new LatLng(-9.6044444444444,-43.116055555556);
        googleMap.addMarker(new MarkerOptions()
                .position(location_1)
                .title("MeliApis")
        ).setTag(1);

        // Location 3
        LatLng location_3 = new LatLng(-9.4605833333333,-40.891694444444);
        Marker marker3 = googleMap.addMarker(new MarkerOptions()
                .position(location_2)
                .title("MeliApis")
                .snippet("clique para mais informações")
        );
        marker3.setTag(2);

        // Location 4
        LatLng location_4 = new LatLng(-9.3157222222222,-41.288611111111);
        googleMap.addMarker(new MarkerOptions()
                .position(location_4)
                .title("MeliApis")
        ).setTag(3);

        // Location 5
        LatLng location_5 = new LatLng(-9.9366944444444,-42.458638888889);
        Marker marker5 = googleMap.addMarker(new MarkerOptions()
                .position(location_5)
                .title("MeliApis")
        );
        marker5.setTag(4);

        // Location 6
        LatLng location_6 = new LatLng(-9.9033055555556,-42.488527777778);
        googleMap.addMarker(new MarkerOptions()
                .position(location_6)
                .title("MeliApis")
        ).setTag(5);

        // Location 7
        LatLng location_7 = new LatLng(-9.9900833333333,-42.489277777778);
        googleMap.addMarker(new MarkerOptions()
                .position(location_7)
                .title("MeliApis")
        ).setTag(6);

        // Location 8
        LatLng location_8 = new LatLng(-9.3205305555556,-41.138833333333);
        googleMap.addMarker(new MarkerOptions()
                .position(location_8)
                .title("MeliApis")
        ).setTag(7);

        // Location 9
        LatLng location_9 = new LatLng(-9.3027166666667,-41.152833333333);
        googleMap.addMarker(new MarkerOptions()
                .position(location_9)
                .title("MeliApis")
        ).setTag(8);

        // Location 10
        LatLng location_10 = new LatLng(-9.3206388888889,-41.29);
        googleMap.addMarker(new MarkerOptions()
                .position(location_10)
                .title("MeliApis")
        ).setTag(9);

        // Location 11
        LatLng location_11 = new LatLng(-9.313777778,-41.12311111);
        googleMap.addMarker(new MarkerOptions()
                .position(location_11)
                .title("MeliApis")
        ).setTag(10);

        // Location 12
        LatLng location_12 = new LatLng(-9.3562111111111,-41.310777777778);
        googleMap.addMarker(new MarkerOptions()
                .position(location_12)
                .title("MeliApis")
        ).setTag(11);

        // Location 13
        LatLng location_13 = new LatLng(-9.2912777777778,-41.312388888889);
        googleMap.addMarker(new MarkerOptions()
                .position(location_13)
                .title("MeliApis")
        ).setTag(12);

        // Location 14
        LatLng location_14 = new LatLng(-9.32075,-41.289972222222);
        googleMap.addMarker(new MarkerOptions()
                .position(location_14)
                .title("MeliApis")
        ).setTag(13);

        // Location 15
        LatLng location_15 = new LatLng(-9.6155277777778,-42.547944444444);
        googleMap.addMarker(new MarkerOptions()
                .position(location_15)
                .title("MeliApis")
        ).setTag(14);

        // Location 16
        LatLng location_16 = new LatLng(-9.5797777777778,-41.979972222222);
        googleMap.addMarker(new MarkerOptions()
                .position(location_16)
                .title("MeliApis")
        ).setTag(15);

        // Location 17
        LatLng location_17 = new LatLng(-9.739886,-42.298853);
        googleMap.addMarker(new MarkerOptions()
                .position(location_17)
                .title("MeliApis")
        ).setTag(16);

        // Location 18
        LatLng location_18 = new LatLng(-9.580249,-41.980155);
        googleMap.addMarker(new MarkerOptions()
                .position(location_18)
                .title("MeliApis")
        ).setTag(17);

        // Location 19
        LatLng location_19 = new LatLng(-9.58,-41.983611111111);
        googleMap.addMarker(new MarkerOptions()
                .position(location_19)
                .title("MeliApis")
        ).setTag(18);

        // Location 20
        LatLng location_20 = new LatLng(-9.5483333333333,-41.976666666667);
        Marker marker20 = googleMap.addMarker(new MarkerOptions()
                .position(location_20)
                .title("MeliApis")
                .snippet("Clique para mais informações!")
        );
        marker20.setTag(19);
        marker20.showInfoWindow();

        // Location 21
        LatLng location_21 = new LatLng(-9.5613888888889,-41.978611111111);
        googleMap.addMarker(new MarkerOptions()
                .position(location_21)
                .title("MeliApis")
        ).setTag(20);

        // Location 22
        LatLng location_22 = new LatLng(-9.5163055555556,-40.861111111111);
        googleMap.addMarker(new MarkerOptions()
                .position(location_22)
                .title("MeliApis")
        ).setTag(21);

        // Location 23
        LatLng location_23 = new LatLng(-10.041972222222,-41.415944444444);
        googleMap.addMarker(new MarkerOptions()
                .position(location_23)
                .title("MeliApis")
        ).setTag(22);


        // Location 24
        LatLng location_24 = new LatLng(-10.040983, -41.4159935);
        googleMap.addMarker(new MarkerOptions()
                .position(location_24)
                .title("MeliApis")
        ).setTag(23);

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(location_20));
        googleMap.setMinZoomPreference(7.0f);

        // [END_EXCLUDE]
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int option = Integer.valueOf(marker.getTag().toString());
        Meliponario meliponario = null;
        Intent intent;
        Log.d("Maps", "marker inte: " + option);
        switch (option){
            case 0:
            case 2:
            case 5:
            case 6:
            case 9:
            case 13:
            case 18:
            case 20:
            case 23:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)\nMandaçaia (Melipona mandacaia)",
                        "Mel"
                );
                break;
            case 1:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)\nMandaçaia (Melipona mandacaia)\nMunduri (Melipona asilvai)",
                        "Mel"
                );
                break;
            case 3:
            case 7:
            case 8:
            case 10:
            case 11:
            case 12:
            case 19:
            case 21:
            case 22:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)",
                        "Mel"
                );
                break;
            case 4:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)\nMandaçaia (Melipona mandacaia)\nMunduri (Melipona asilvai)\nabelha branca (Frieseomelitta doederleini)",
                        "Mel"
                );
                break;
            case 14:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)",
                        "Mel\nMel em favo"
                );
                break;
            case 15:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)\nMandaçaia (Melipona mandacaia)",
                        "Mel\nMolho de pimenta com mel"
                );
                break;
            case 16:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)\nMandaçaia (Melipona mandacaia)",
                        "Mel\nHidromel\nPrópolis"
                );
                break;
            case 17:
                meliponario = new Meliponario(
                        option,
                        marker.getPosition().latitude,
                        marker.getPosition().longitude,
                        "Abelha africanizada (Apis mellifera)\nMandaçaia (Melipona mandacaia)",
                        "Mel\nGeoprópolis\nExtrato de própolis"
                );
                break;
        }
        if (meliponario != null) {
            intent = new Intent(this, MarkActivity.class);
            intent.putExtra("meliponario", meliponario);
            startActivity(intent);
        }
       // Log.e("MAPS", "marker TAG:" + marker.getTag());
        return true;
    }
    // [END maps_marker_on_map_ready_add_marker]
}
// [END maps_marker_on_map_ready]
